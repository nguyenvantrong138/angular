# Directive 

Directive là một thành phân mở rộng HTML, hay nói cách khác thuộc tính (Properties) của các thẻ HTML  mà Angular nó định nghĩa thêm, vì nó là của riêng Angular nên phải tuân thủ theo nguyên tắc của nó là chữ bắt đầu luôn luôn là ký tự...........

Ngoài những directive được [AngularJS cung cấp sẵn](https://docs.angularjs.org/api/ng#directive), chúng ta hoàn toàn có thể tự [custom directive](https://docs.angularjs.org/guide/directive) phù hợp với mục đích sử dụng.

## Component 

Component là thành phần đặc biệt của directive ,
